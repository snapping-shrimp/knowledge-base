# Projects

## RobotJS

Repository: https://github.com/octalmage/robotjs

Website: https://robotjs.io

Kind: Library

Input/Output:

- Control Mouse
- Control Keyboard
- Read Screen

Platforms:

- Mac
- Windows
- Linux

Projects built with it: https://github.com/octalmage/robotjs/wiki/Projects-using-RobotJS

Tried?: No


